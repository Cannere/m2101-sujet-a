/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse de Trames GPS                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : CANNERE Xavier                                               *
*                                                                             *
*  Nom-prénom2 : WATTRE Benjamin                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                         *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char * veriftrame(char * ch, char extraction[][100]) {

    // Chaine commence par $GPGGA
    if ((ch[0]=='$') && (ch[1]=='G') && (ch[2]=='P') && (ch[3]=='G') && (ch[4]=='G') && (ch[5]=='A'))  {
    printf("Type de trame correct\n");
    }
    else {
    printf("Trame incorrecte\n");
    exit(0);
    }



    // Chaîne ayant plus de 15 champs et extraction de ces champs
    char delimitateur[]=",";
    char *p = strtok(ch, delimitateur);
    int i=0;
    while(p != NULL) {
        strcpy (extraction[i], p);
        p = strtok(NULL, delimitateur);
        i++;
    }

    if (i >= 5) {
    printf("Trame correcte\n");
    printf("La trame est bien de type %s\n", ch); // printf qui permet de contrôler qu'il n'y ai pas d'erreur jusqu'à cette étape
    }
    else {
    printf("Trame incorrecte\n");
    exit(0);
    }
}

// fonction qui convertit les coordonnées GPS
char * convertpos(char extraction[][100], char * flatitude, char * flongitude) {

    double latitude = atof(extraction[2]);
    double longitude = atof(extraction[4]);

    int degl, minl, degll, minll;
    double secl, secll;

    //latitude
    degl=(int)(latitude/100.0);
    minl=(int)(latitude-(degl*100.0));
    secl=60.0*(latitude-(degl*100.0)-minl);

    sprintf(flatitude,"%3d%c%2d'%5.3f",degl,0xF8,minl,secl);

    //longitude
    degll=(int)(longitude/100.0);
    minll=(int)(longitude-(degll*100.0));
    secll=60.0*(longitude-(degll*100.0)-minll);

    sprintf(flongitude,"%3d%c%2d'%5.3f",degll,0xF8,minll,secll);

    //renvoi des coordonnées
    return flatitude;
    return flongitude;
}

//fonction qui convertit le temps en h/m/s
char * convertmin (char extraction[][100], char * ftemps) {
	char temps[25];
	int heures;
	int minutes;
	double secondes;
	strcpy(temps, extraction[1]);
	double temps2 = atof(temps);
	heures = (int)temps2 /10000;
	minutes = (int)(temps2 - (heures * 10000)) / 100;
	secondes = (temps2 - (heures * 10000) - (minutes * 100));
	sprintf(ftemps, "%dh%dmin%lfsec \n", heures, minutes, secondes);
	return ftemps;
}

// fonction main
void main() {
    char *ch=malloc(99*sizeof(char));
    char *flongitude=malloc(99*sizeof(char));
    char *flatitude=malloc(99*sizeof(char));
    char *ftemps=malloc(99*sizeof(char));
    char extraction[20][100]={0};
    printf("Veuillez entrer la trame : ");
    scanf("%s", ch);
    veriftrame(ch, extraction);
    convertpos(extraction, flatitude, flongitude);
    convertmin(extraction, ftemps);
    printf("L'heure %c laquelle la trame a %ct%c envoy%ce est : %s", 133, 130, 130, 130, ftemps);
    printf("La latitude est : %s %s\n", flatitude, extraction[3]);
    printf("La longitude est : %s %s\n", flongitude, extraction[5]);
    // ecriture du fichier "resultat"
    FILE* fichier = NULL;
    fichier = fopen("resultat","w");
    if (fichier == NULL) {
        printf("Erreur à l'ouverture du fichier texte\n");
    }
    else {
        fprintf(fichier,"L'heure à laquelle la trame a été envoyée est : %s", fichier, ftemps);
        fprintf(fichier,"La latitude est : %s %s\n", fichier, flatitude, extraction[3]);
        fprintf(fichier,"La longitude est : %s %s\n", fichier, flongitude, extraction[5]);
        fclose(fichier);
    }
}
