**TOOL FOR CONVERTING A GPS FRAME**

FONCTION 1 - veriftrame :

Elle permet de verifier que la trame est bien au format $GPGGA
et contient plus de 15 champs

FONCTION 2 - convertpos :

La fonction convertpos permet de convertir la longitude et la latitude qui
sont au format d'un tableau de char bidimensionnel au format int ou double

FONCTION 3 - convertmin :

Même principe que convertpos mais convertit l'heure à laquelle
la trame a été envoyée

lien git : https://framagit.org/Cannere/m2101-sujet-a
